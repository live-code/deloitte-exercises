export interface Product{
    id: number;
    label: string;
    cost: number;
    layout?: Layout
}

export type Layout = "A" | "B" | "AA"