import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterOutlet } from '@angular/router';
import { NavBarComponent } from "./core/nav-bar.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, RouterLink, NavBarComponent],
  template: `
    <app-nav-bar/>
    <router-outlet></router-outlet>
  `,
  styles: [],
})
export class AppComponent {
  title = 'deloitte-exercises';
}
