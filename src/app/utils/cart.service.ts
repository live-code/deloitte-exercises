import { Injectable } from '@angular/core';
import { Product } from "../models/product";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: Product[] = [
    {
      id: 1,
      label: "item A",
      cost: 10,
    },
    {
      id: 2,
      label: "item B",
      cost: 20,
    }
  ];

  constructor() { }

  get total(){
    return this.items.length
  }

  get totalCost(){
    let total = 0;
    this.items.forEach(item => {
      total += item.cost 
    })
    return total
  }

  addToCart(product: Product){
    this.items.push(product);

  }
}
