import { Injectable } from '@angular/core';
import { Post } from "../features/demo2.component";
import { HttpClient } from "@angular/common/http";
import { forkJoin, mergeMap } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DbService {
  value = 123; 
 

  constructor(private http: HttpClient) { }

  getPost(id: number){
    
    return this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${id}`)
  
  }

  getAll(){
    return this.http.get<Post>('https://jsonplaceholder.typicode.com/posts/50')
    .pipe(
      mergeMap(post => forkJoin([
        this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts?userId='+ post.userId),
        this.http.get<any[]>('https://jsonplaceholder.typicode.com/users?userId='+ post.userId)
      ]))
    )
  }
}
