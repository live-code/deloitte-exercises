import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { concatMap, debounceTime, filter, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  template: `
    <p>
      demo1 works!
      <input [formControl]="input" type="text">
      {{result$ | async | json}}
    </p>
    <li *ngFor="let user of result$ | async">{{user.name}}</li>
  `,
  styles: [
  ]
})
export class Demo1Component {
  input = new FormControl("", { nonNullable: true })
  http = inject(HttpClient)

  result$ = this.input.valueChanges
    .pipe(
      filter(value => value.length >= 2),
      debounceTime(500),
      concatMap(text =>
        this.http.get<any[]>('https://jsonplaceholder.typicode.com/users?q=' + text)
      )
    )
}

