import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { combineLatest, filter, mergeMap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DbService } from "../utils/db.service";

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  template: `
    <p>
      demo3 works!
    </p>
    <select [formControl]="select1" >
      <option value="" >Select value 1</option>
      <option value="A"> A </option>
      <option value="B"> B </option>
    </select>
    <select [formControl]="select2" >
      <option value="" >Select value 2</option>
      <option value="1"> 1 </option>
      <option value="2"> 2 </option>
    </select>
  `,
  styles: [
  ]
})
export default class Demo3Component {
  
  select1 = new FormControl("", {nonNullable: true})
  select2 = new FormControl("", {nonNullable: true})
  a = "xyz";

  constructor(private http: HttpClient, private dbService: DbService){

    const a = 123;
    console.log(dbService.value);

    // this.select1.valueChanges
    // .pipe(
    //   filter((value) => value.length >0 )
    // )
    // .subscribe(value => {
    //   console.log(value)
    // })
    combineLatest([
      this.select1.valueChanges,
      this.select2.valueChanges
    ])
    .pipe(
      mergeMap((value) => this.http.get<any[]>("" + value))
    )
    .subscribe(value => {
        console.log(value)
      })
  }

  doSomething(){
    this.dbService

  }

}
