import { Component, Directive, Input, SimpleChanges, ViewContainerRef } from '@angular/core';
import { Layout } from 'src/app/models/product';
import { LayoutAComponent } from './layout-a.component';
import { LayoutBComponent } from './layout-b.component';

@Directive({
  selector: '[appLayoutSwitcher]',
  standalone: true,
})
export class LayoutSwitcherComponent {
  @Input({required:true}) appLayoutSwitcher:Layout | undefined;
  
  constructor(private view:ViewContainerRef){
  }

  ngOnChanges(changes:SimpleChanges){
    console.log('LAYOUT CHANGED:', changes['layout'])
    this.view.clear() // logs when the input value is changed
    if(this.appLayoutSwitcher){
      const component = this.view.createComponent(components[this.appLayoutSwitcher].component)
      component.instance.titolo = components[this.appLayoutSwitcher].title

    }
  }
}

const components = {
  A:{ 
    component: LayoutAComponent,
    title: "Titolo A"
  },
  B:{ 
    component: LayoutBComponent,
    title: "Titolo BBBBBBB"
  },
  AA:{ 
    component: LayoutAComponent,
    title: "Titolo AA"
  },
}