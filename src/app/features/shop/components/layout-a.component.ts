import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-layout-a',
  standalone: true,
  imports: [CommonModule],
  template: `
   {{titolo}}
   
   <button>Clicca</button>
   <button>Annulla</button>
  `,
  styles: [
  ]
})
export class LayoutAComponent {
  @Input() titolo:string|undefined
}
