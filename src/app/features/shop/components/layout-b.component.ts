import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-layout-b',
  standalone: true,
  imports: [CommonModule],
  template: `
  {{titolo}}
    <p>
      layout-b works!
    </p>
  `,
  styles: [
  ]
})
export class LayoutBComponent {
  @Input() titolo:string | undefined;
}
