import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from "@angular/common/http";
import { Product } from "src/app/models/product";
import { CartService } from "src/app/utils/cart.service";
import { ModalModule } from 'src/app/shared/modal.module';
import { LayoutSwitcherComponent } from './components/layout-switcher.component';

@Component({
  selector: 'app-shop',
  standalone: true,
  imports: [CommonModule,ModalModule,LayoutSwitcherComponent],
  template: `

  
  <app-modal title="Titolo">
    <app-modal-footer >
      <pre>{{selectedProduct?.layout | json}}</pre>
      <div [appLayoutSwitcher]="selectedProduct?.layout"></div>
    </app-modal-footer>
    <app-modal-body>
      <li *ngFor="let product of products">
      <span (click)="selectedProduct = product">
        {{product.label}} 
      </span>
      <button (click)="cartService.addToCart(product)"> + </button>

    </li>
    </app-modal-body>
  </app-modal>
  `,
  styles: [
  ]
})
export default class ShopComponent implements OnInit {

  products: Product[] = [];
  selectedProduct:Product | undefined
  ngOnInit(): void {
 
    this.http.get<Product[]>(" http://localhost:3000/prodotti")
    .subscribe(result =>{ 
      this.products = result;
      console.log(result)
    })
  }

  http = inject(HttpClient)
  cartService = inject(CartService)



}
