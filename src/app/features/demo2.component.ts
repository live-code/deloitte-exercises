import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import a, { pluto } from '../utils/math.utils';
import { HttpClient } from '@angular/common/http';
import { forkJoin, interval, mergeMap, of } from 'rxjs';
import { DbService } from "../utils/db.service";


@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      demo2 works!
    </p>
  `,
  styles: [
  ]
})
export default class Demo2Component {
  http = inject(HttpClient)
  constructor(dbService: DbService){

    dbService.getPost(34)
    .subscribe(result => console.log("Result", result))

   dbService.getAll()
    .subscribe(result => console.log("Result", result))
  }

}

export interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}
export interface User {
  id: number;
  name: string;
}


