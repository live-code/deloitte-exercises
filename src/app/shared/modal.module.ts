import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalBodyComponent } from './modal-body.component';
import { ModalFooterComponent } from './modal-body.component copy';
import { ModalComponent } from './modal.component';



@NgModule({
  declarations: [],
  imports: [CommonModule,ModalComponent,ModalBodyComponent,ModalFooterComponent],
  exports: [ModalComponent,ModalBodyComponent,ModalFooterComponent],

})
export class ModalModule { }
