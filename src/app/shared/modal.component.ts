import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [CommonModule],
  template: `
   <div class="card">
    <div class="card-header">
      {{title}}
    </div>
    <div class="card-body">
      <ng-content select="app-modal-body"></ng-content>
    </div>
    <div class="card-footer">
      <ng-content select="app-modal-footer"></ng-content>
    </div>
   </div>
  `,
  styles: [
  ]
})
export class ModalComponent {
  @Input({required:true}) title:string | undefined;
}
