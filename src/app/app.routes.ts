import { Routes } from '@angular/router';

export const routes: Routes = [
    {path: 'demo1', loadComponent: () => import('./features/demo1.component').then(m => m.Demo1Component)},
    {path: 'demo2', loadComponent: () => import('./features/demo2.component')},
    {path: 'demo3', loadComponent: () => import('./features/demo3.component')},
    {path: 'shop', loadComponent: () => import('./features/shop/shop.component')}
];
