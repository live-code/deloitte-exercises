import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from "@angular/router";
import { CartService } from "../utils/cart.service";

@Component({
  selector: 'app-nav-bar',
  standalone: true,
  imports: [CommonModule, RouterLink],
  template: `

    
    <button [routerLink]="'demo1'" > Demo1 </button>
    <button [routerLink]="'demo2'" > Demo2 </button>
    <button [routerLink]="'demo3'" > Demo3 </button>
    <button [routerLink]="'shop'" > Shop </button>
    <span>{{cartService.total}} - </span>
    <span>{{cartService.totalCost}}</span>
  `,
  styles: [
  ]
})
export class NavBarComponent {

  constructor(public cartService: CartService){

  }

}
